{-# LANGUAGE TemplateHaskell #-}

module Game.NetFlak.Types.EnvironmentVariables where

import Game.NetFlak.Types.SpecialArea

import Lens.Simple

data EnvironmentVariables = EnvironmentVariables 
  { _difficulty  :: Integer
  , _specialArea :: SpecialArea
  } deriving Show
makeLenses ''EnvironmentVariables

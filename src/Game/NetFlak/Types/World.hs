{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.World where

import Lens.Simple

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Settings

data World a = World
  { _player        :: Player
  , _currentLevel  :: Level a
  , _currentSpell  :: Spell
  , _deadEnemies   :: [(Vector, Enemy)]
  , _bloodTracks   :: [(Vector, RichString)]
  , _rawLog        :: [LogItem]
  , _richLog       :: [RichString]
  , _userSettings  :: Settings
  }
  deriving
    ( Show
    , Eq
    )
makeLenses ''World

module Game.NetFlak.Types.SpecialArea where

data SpecialArea
  = Normal
  | Alchemist
  | Statue
  deriving
    ( Show
    , Eq
    )


{-# Language TemplateHaskell #-}

module Game.NetFlak.Types.BuildState where

import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.Potion

import Lens.Simple

data AddUI a
  = Select Integer
  | Place a Vector

data Adding
  = SpellComponent
  | Enemy
  | Potion (Maybe (AddUI Potion))

data BuildUI
  = QuitBuild
  | MovingPlayer
  | ResizeLevel
  | MovingDoor
  | Adding (Maybe Adding)
  | Editing
  | Removing

data BuildState = BuildState
  { _building      :: PuzzleLevel
  , _buildUIState  :: BuildUI
  , _buildSettings :: Settings
  }
makeLenses ''BuildState

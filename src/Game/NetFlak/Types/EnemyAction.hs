module Game.NetFlak.Types.EnemyAction where

import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.Potion

data CauseOfDeath
  = PlayerKill
  | Trampled
  | InWall
  deriving
    ( Eq
    , Show
    )

data EnemyAction
  = MoveAction Vector
  | AttackAction Integer [ Potion ]
  | Death CauseOfDeath
  | NoAction
  | GrowthAction Integer
  deriving Eq


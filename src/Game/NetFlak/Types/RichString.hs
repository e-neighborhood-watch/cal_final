module Game.NetFlak.Types.RichString where

import Data.List (genericLength)

import Control.Arrow (first)

import Game.NetFlak.Splittable

import Game.NetFlak.Types.PseudoColor

newtype RichString = RichString [(PseudoColorID, String)] deriving Eq

instance Show RichString where
  show (RichString l) = l >>= snd 

instance Splittable RichString where
  genericSplitAt i a
   | i < 0 = (RichString [], a)
  genericSplitAt i (RichString []) = (RichString [], RichString [])
  genericSplitAt i (RichString ((xC, "") : xs)) =
    genericSplitAt i (RichString xs)
  genericSplitAt i (RichString ((xC, xS) : xs)) =
    first (liftRich2 (++) $ RichString $ [(xC, genericTake i xS)]) $ genericSplitAt (i - genericLength xS) (RichString ((xC, genericDrop i xS) : xs))

liftRich2 :: ([(PseudoColorID, String)] -> [(PseudoColorID, String)] -> [(PseudoColorID, String)]) -> RichString -> RichString -> RichString
liftRich2 f (RichString r1) (RichString r2) = RichString $ f r1 r2


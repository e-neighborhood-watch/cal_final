{-# LANGUAGE TemplateHaskell #-}

module Game.NetFlak.Types.Level where

import Lens.Simple

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.SpecialArea
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.Vector

type PuzzleLevel = Level ()

type EndlessLevel = Level (Maybe SpecialArea)

data Level a = Level 
  { _levelSize       :: Vector
  , _playerLocation  :: Vector
  , _floorComponents :: [(Vector, SpellComponent)]
  , _floorPotions    :: [(Vector, Potion)]
  , _enemies         :: [(Vector, Enemy)]
  , _exitDoors       :: [Door a]
  , _startMessages   :: [[(PseudoColorID, String)]]  -- TODO make this rich strings
  }
  deriving
    ( Show
    , Read
    , Eq
    )
makeLenses ''Level

module Game.NetFlak.Types.LogItem where

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.SpecialArea
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Potion

data LogItem
  = EnemyAttack Enemy Integer [Potion]
  | SpellCast Spell
  | TimeOut
  | UnbalancedBraces
  | Pickup PseudoColorID String
  | UsePotion Potion
  | BadPotion Potion
  | CannotUse RichString RichString
  | EnemyDeath CauseOfDeath Enemy
  | StatueAwakens Enemy
  | DoorOpens
  | DistantDoorOpens
  | PlayerDies
  | AreaBanner SpecialArea
  deriving
    ( Show
    , Eq
    )

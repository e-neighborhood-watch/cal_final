module Game.NetFlak.Types.EnemyMemory where

data EnemyMemory
  = Vertical
  | Horizontal
  | NoMovement
  deriving (
      Show
    , Read
    , Eq
    )

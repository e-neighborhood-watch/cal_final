{-# LANGUAGE MultiParamTypeClasses, ScopedTypeVariables #-}

module Game.NetFlak.StateTransfer.Play (quitEvent, uiState, nextState) where

import Lens.Simple

import Control.Monad.Random
import Control.Monad.State

import Data.List (genericLength)
import Data.Map (insert)
import Data.Map.Strict (toList)
import Data.Maybe (fromMaybe)

import qualified Data.Map as Map

import Game.NetFlak.Controls
import Game.NetFlak.Level
import Game.NetFlak.Logs
import Game.NetFlak.ProgramMode
import Game.NetFlak.Spell
import Game.NetFlak.TakeStep
import Game.NetFlak.Util (maybeIndex)
import Game.NetFlak.World

import Game.NetFlak.Draw.Lookable

import Game.NetFlak.Render.Play

import Game.NetFlak.Types.Control
import Game.NetFlak.Types.CurseEvent
import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.PlayState
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.World

import Graphics.Vty

instance Quittable (UIState genT a) where
  quitEvent Quit = True
  quitEvent _    = False

newLevel :: Level a -> World a -> World a
newLevel next world = buildWorld next (view player world) (view userSettings world)

updateLog ::
  MonadRandom m
    => World a -> m (World a)
updateLog world = do
  newRichLogs <- concat <$> mapM fillLog (view rawLog world)
  return $ over richLog (newRichLogs ++) $ set rawLog [] world

instance Lookable link => ProgramMode (PlayState genT link) (UIState genT link) (World link) where
  uiState = playUIState
  render = renderPlay
  renderData = currentWorld
  nextState event (constState, gen)
    | Prompt{} <- view uiState newPlayState = (                            newPlayState, newGen)
    | Quit     <- view uiState newPlayState = (                            newPlayState, newGen)
    | playerDead                            = (set uiState deadPrompt      newPlayState, newGen)
    | otherwise                             = flip (,) newGen $
      case
        -- Doors that the player is standing on
        filter ((== view (currentWorld . currentLevel . playerLocation) newPlayState) . view doorPosition) $
          -- And are open
          filter (view isOpen) $
            view (currentWorld . currentLevel . exitDoors) newPlayState
        of
          []         -> newPlayState
          (door : _) -> set uiState (nextLevelPrompt $ view destination door) newPlayState
    where
      world :: World link
      world = view currentWorld constState

      numSide :: Num n => Bool -> n
      numSide True  = genericLength $ view (player . playerInv . heldComponents) world
      numSide False = genericLength $ toList $ view (player . playerInv . heldPotions) world

      playerDead :: Bool
      playerDead = view (currentWorld . player . damageTaken) newPlayState >= view (currentWorld . player . maxHealth) newPlayState

      quitPrompt, deadPrompt :: UIState genT link
      quitPrompt = Prompt [
          RichString [(DefaultPID, if view (userSettings . permadeath) world
            then "Quit game or continue?"
            else "Quit game, reset level or continue?")]
        ] (view uiState constState) $ if view (userSettings . permadeath) world
          then
            [ ('q', set uiState Quit constState)
            , ('c', constState)
            ]
          else
            [ ('q', set uiState Quit constState)
            , ('r', set uiState WalkMode $ set currentWorld (view save newPlayState) newPlayState)
            , ('c', constState)
            ]
      deadPrompt = Prompt
        [ RichString [(DefaultPID, "You died")]
        ] (view uiState newPlayState) $ if view (userSettings . permadeath) world
          then
            [ ('q', set uiState Quit newPlayState)
            ]
          else
            [ ('q', set uiState Quit newPlayState)
            , ('r', set uiState WalkMode $ set currentWorld (view save newPlayState) newPlayState)
            ]
      nextLevelPrompt link =
        case runStateT (view nextLevel newPlayState link) (view curLevelGen newPlayState) of
          Nothing -> Prompt
            [ RichString [ (DefaultPID, "You won!") ]
            ] (view uiState newPlayState)
              [ ('y', set uiState Quit newPlayState)
              ]
          Just (gennedLevel, newCurLevelGen) ->
            let nextLevelWorld = newLevel gennedLevel $ view currentWorld newPlayState
            in Prompt
              [ RichString [(DefaultPID, "Continue to next level?")]
              ] (view uiState newPlayState)
                [ ( 'y'
                  , set curLevelGen newCurLevelGen $
                    set save nextLevelWorld $
                      set currentWorld nextLevelWorld $
                        set uiState WalkMode newPlayState
                  )
                , ( 'n'
                  , over currentWorld performPickUp $ 
                    over (currentWorld . currentLevel) removeFromDoor newPlayState
                  )
                ]

      totalOptions :: Num p => p
      totalOptions = 5

      control :: Control
      control = getControl (view userSettings world) event

      setUIStateWorld :: (UIState genT link, World link) -> PlayState genT link
      setUIStateWorld = uncurry $ flip (set currentWorld) . setUIState

      setUIState :: UIState genT link -> PlayState genT link
      setUIState = flip (set uiState) constState

      (newPlayState, newGen) = over _1 (\w -> set currentWorld w foobar) $ runRand (updateLog $ view currentWorld foobar) gen
      foobar = 
        case view uiState constState of
          WalkMode -> setUIStateWorld $
            case control of
              UseHotkey key  -> (WalkMode, useHotbarComponent key world)
              Move disp      -> (WalkMode, movePlayer disp world)
              QuitGame       -> (quitPrompt, world)
              CastSpell      -> (WalkMode, executeSpell world)
              ClearSpell     -> (WalkMode, clearSpell world)
              ToggleLook     -> (LookMode $ view (currentLevel . playerLocation) world, world)
              ToggleInv      -> (InventoryMode (numSide True > 0) 0, world)
              ToggleEnemy    -> (EnemyMode 0, world)
              ToggleMessages -> (MessagesMode 0, world)
              ToggleSettings -> (SettingsMode 0, world)
              ToggleControls -> (ControlsMode, world)
              UseQuickPotion -> (WalkMode, useQuickPotion world)
              somethingElse  -> (view uiState constState, world)
          ControlsMode -> setUIState $
            case control of
              ExitMode       -> WalkMode
              ToggleControls -> WalkMode
              ToggleLook     -> LookMode $ view (currentLevel . playerLocation) world
              ToggleInv      -> InventoryMode (numSide True > 0) 0
              ToggleEnemy    -> EnemyMode 0
              ToggleMessages -> MessagesMode 0
              ToggleSettings -> SettingsMode 0
              QuitGame       -> quitPrompt
              somethingElse  -> ControlsMode
          LookMode center -> setUIState $
            case control of
              Move disp      -> LookMode $ center + disp
              ExitMode       -> WalkMode
              ToggleLook     -> WalkMode
              ToggleInv      -> InventoryMode (numSide True > 0) 0
              ToggleEnemy    -> EnemyMode 0
              ToggleMessages -> MessagesMode 0
              ToggleSettings -> SettingsMode 0
              ToggleControls -> ControlsMode
              QuitGame       -> quitPrompt
              somethingElse  -> LookMode center
          EnemyMode index -> setUIState $
            case control of
              ExitMode       -> WalkMode
              ToggleEnemy    -> WalkMode
              ToggleLook     -> LookMode $ view (currentLevel . playerLocation) world
              ToggleInv      -> InventoryMode (numSide True > 0) 0
              ToggleMessages -> MessagesMode 0
              ToggleSettings -> SettingsMode 0
              ToggleControls -> ControlsMode
              QuitGame       -> quitPrompt
              Menu x         -> EnemyMode $
                                max 0 $
                                  min (subtract 1 $ genericLength $ enemiesInBounds $ view currentLevel world) $
                                    index + x
              somethingElse -> EnemyMode index
          InventoryMode side index -> setUIStateWorld $
            case control of
              ExitMode       -> (WalkMode, world)
              ToggleLook     -> (LookMode $ view (currentLevel . playerLocation) world, world)
              ToggleInv      -> (WalkMode, world)
              ToggleEnemy    -> (EnemyMode 0, world)
              ToggleMessages -> (MessagesMode 0, world)
              ToggleSettings -> (SettingsMode 0, world)
              ToggleControls -> (ControlsMode, world)
              Menu x         -> (InventoryMode side $ max 0 $ min (numSide side - 1) (index + x), world)
              MenuToggle     | numSide (not side) > 0 -> (InventoryMode (not side) $ min (numSide (not side) - 1) index, world)
              MenuSelect     | side     -> (InventoryMode side index, takeStep $ useSpellAtIndex index world)
              MenuSelect     | not side -> let newWorld = useItemAtIndex index world in
                (InventoryMode side $ min index $ genericLength (toList $ view (player . playerInv . heldPotions) newWorld) - 1, newWorld)
              UseHotkey key  | side -> (InventoryMode side index, over (player . playerInv . hotbarMap) (insert key index) world)
              QuitGame       -> (quitPrompt, world)
              UseQuickPotion
                | not side   -> (InventoryMode side index, set (player . playerInv . quickPotion) (maybeIndex (fst <$> toList (view (player . playerInv . heldPotions) world)) index) world)
              somethingElse  -> (InventoryMode side index, world)
          MessagesMode offset -> setUIState $
            case control of
              ExitMode       -> WalkMode
              ToggleEnemy    -> EnemyMode 0
              ToggleLook     -> LookMode $ view (currentLevel . playerLocation) world
              ToggleInv      -> InventoryMode (numSide True > 0) 0
              ToggleMessages -> WalkMode
              ToggleControls -> ControlsMode
              Menu x         -> MessagesMode $ max 0 $ offset + x
              QuitGame       -> quitPrompt
              somethingElse  -> MessagesMode offset
          SettingsMode index -> setUIStateWorld $
            case control of
              ExitMode       -> (WalkMode, world)
              ToggleLook     -> (LookMode $ view (currentLevel . playerLocation) world, world)
              ToggleInv      -> (InventoryMode (numSide True > 0) 0, world)
              ToggleEnemy    -> (EnemyMode 0, world)
              ToggleMessages -> (MessagesMode 0, world)
              ToggleSettings -> (WalkMode, world)
              ToggleControls -> (ControlsMode, world)
              Menu x         -> (SettingsMode $ min totalOptions $ max 0 (index + x), world)
              -- TODO clamp UI elements to level size
              Crement n      -> (,) (SettingsMode index) $ case index of
                0 -> over (userSettings . maxSpellCycles) (max 0 . (+n)) world
                1 -> over (userSettings . hotbarWidth)    (max 0 . (+n)) world
                2 -> over (userSettings . spellbarWidth)  (max 0 . (+n)) world
                3 -> over (userSettings . messageWidth)   (max 0 . (+n)) world
                4 -> over (userSettings . enemybarWidth)  (max 0 . (+n)) world
                5 -> over (userSettings . infobarWidth)   (max 0 . (+n)) world
              QuitGame       -> (quitPrompt, world)
              somethingElse  -> (SettingsMode index, world)
          Prompt _ _ outgoing
            | (KChar key,[]) <- event -> fromMaybe constState $ lookup key outgoing
            | otherwise               -> constState
          Quit -> constState

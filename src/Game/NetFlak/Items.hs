module Game.NetFlak.Items (Item(..), amountUsed) where

import Prelude hiding (lookup)

import Lens.Simple

import Data.List hiding (lookup)
import Data.Map (lookup)
import Data.Char (toUpper)

import Game.NetFlak.Color

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.World

amountUsed :: (Integral a) => ComponentStructure -> Spell -> a
amountUsed structure spell
  = case lookup structure $ view usedComps spell of
    Nothing     -> 0
    Just amount -> fromInteger amount

-- Splits off the longest prefix of the string that is either all Xs or no Xs
splitXs :: String -> (String, String)
splitXs "" = ("", "") 
splitXs string@('X' : _) = break (/='X') string
splitXs string           = break (=='X') string

-- Makes the description for a part of a spell component either all Xs or no Xs
basicDesc :: String -> String
basicDesc "X" = "deletes a character"
basicDesc string
 | all (== 'X') string = "deletes " ++ show (length string) ++ " characters from the end of your spell"
 | otherwise           = "adds " ++ string ++ " to the end of your spell"

capitalize :: String -> String
capitalize "" = ""
capitalize (a : b) = toUpper a : b

-- Makes the full description for the old style of Unlimited spell component 
-- Could be done as a fold?
compDesc :: String -> String
compDesc = capitalize . go
  where
    go rem = case splitXs rem of
      (_, "")       -> basicDesc rem ++ "."
      (start, next) -> basicDesc start ++ " then " ++ go next

class Item a where
  itemColor   :: a -> PseudoColorID
  itemName    :: a -> String
  itemDesc    :: a -> String
  itemIcon    :: a -> Char
  canUse      :: a -> World b -> Maybe LogItem
  richName    :: a -> RichString
  richName a = RichString $ [(itemColor a, itemName a)]

instance Item SpellComponent where
  itemColor (UnlimitedSC _) = GreenPID
  itemColor (LimitedSC _ _) = BluePID

  itemName (UnlimitedSC [""]) = "e"
  itemName (UnlimitedSC x)
    -- Trim a space at the beginning
    = case compName x of
      (' ' : x) -> x
      x         -> x
    where
      -- Takes a ComponentStructure and turns it into a usable string
      compName :: ComponentStructure -> String
      compName [a] = a
      compName ("" : b) = " " ++ "//" ++ compName b
      compName (a  : b) =  a  ++ "//" ++ compName b
      compName [] = "e"
  itemName (LimitedSC _ x) = itemName $ UnlimitedSC x

  itemIcon x = case filter (`notElem` " /") (itemName x) of
    (a : b) -> a 
    ""      -> case itemName x of
      (a : b) -> a
      ""      -> 'e' -- Should never happen !

  itemDesc (UnlimitedSC []) = "Clears the current spell."
  itemDesc (UnlimitedSC [""]) = "Clears the current spell."
  itemDesc (UnlimitedSC [s]) = "Replaces the current spell with" ++ s ++ "."
  itemDesc (UnlimitedSC ["", ""]) = "Does Nothing."
  itemDesc (UnlimitedSC ["",  s]) = compDesc s
  itemDesc (UnlimitedSC [s,  ""]) = "Adds " ++ s ++ " to the beginning of your spell."
  itemDesc (UnlimitedSC contents)
    | all null contents = "Duplicates the spell " ++ show (length contents - 1) ++ " times."
  -- TODO give a meaningful description
  itemDesc (UnlimitedSC contents) = "Does " ++ itemName (UnlimitedSC contents) ++ "."
  itemDesc (LimitedSC 1 contents ) = itemDesc (UnlimitedSC contents) ++ " Can only be used once in a single spell."
  itemDesc (LimitedSC uses contents) = itemDesc (UnlimitedSC contents) ++ " Can only be used " ++ show uses ++ " times in a single spell."

  canUse (UnlimitedSC _) _ = Nothing
  canUse i@(LimitedSC maxUse structure) world
    | maxUse > currentUse = Nothing
    | otherwise           = Just (CannotUse (richName i) (RichString [(DefaultPID, "you are out of uses")]))
    where currentUse = amountUsed structure $ view currentSpell world

instance Item Potion where
  itemColor HealPot     = MagentaPID
  itemColor HPUp        = RedPID
  itemColor Noitop      = RedPID
  itemColor Mirror      = CyanPID
  itemColor GrowthPot   = GreenPID
  itemColor WitherPot   = BluePID
  itemColor CollatzPot  = YellowPID
  itemColor StrengthPot = YellowPID
  itemColor WeakPot     = BlackPID
  itemColor KeyPot      = GreyPID
  itemColor TimePot     = CyanPID

  itemName HealPot     = "Restoring Liniment"
  itemName HPUp        = "Health Elixir"
  itemName Noitop      = "Noitop"
  itemName Mirror      = "Serum of Mirrors"
  itemName GrowthPot   = "Liquor of Growth"
  itemName WitherPot   = "Potion of Withering"
  itemName CollatzPot  = "Collatz Cider"
  itemName StrengthPot = "Liquid Courage"
  itemName WeakPot     = "Liquid Fear"
  itemName KeyPot      = "Cup of Key"
  itemName TimePot     = "Drink of Time"

  itemIcon _ = '&'

  itemDesc HealPot     = "It heals one point of damage."
  itemDesc HPUp        = "It increases max health by one point."
  itemDesc Noitop      = "It reverses your current spell."
  itemDesc Mirror      = "It mirrors your current spell."
  itemDesc GrowthPot   = "It increases the health of all enemies in the room by 1."
  itemDesc WitherPot   = "It decreases the health of all enemies in the room by 1."
  itemDesc CollatzPot  = "It halves the health of all enemies with even health, and triples and increments their health otherwise."
  itemDesc StrengthPot = "It reduces the damage of all enemies in the room by 1."
  itemDesc WeakPot     = "It increases the damage of all enemies in the room by 1."
  itemDesc KeyPot      = "It unlocks all doors in a room."
  itemDesc TimePot     = "It allows you to make an extra action before the enemies next move."

  canUse i@HealPot world
    | view (player . damageTaken) world > 0 = Nothing
    | otherwise
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "you have no health to heal")]))
  canUse i@GrowthPot world
    | null $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@WitherPot world
    | null $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@CollatzPot world
    | null $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@StrengthPot world
    | null $ filter ((> 0) . view (_2 . enemyDamage)) $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@WeakPot world
    | null $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@TimePot world
    | null $ view (currentLevel . enemies) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there is nothing to use it on")]))
  canUse i@KeyPot world
    | null              $ view (currentLevel . exitDoors) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "there are no doors")]))
    | all (view isOpen) $ view (currentLevel . exitDoors) world
      = Just (CannotUse (richName i) (RichString [(DefaultPID, "all the doors are already open")]))
  canUse _ _ = Nothing


isLimited :: SpellComponent -> Bool
isLimited (UnlimitedSC _) = False
isLimited (LimitedSC _ _) = True

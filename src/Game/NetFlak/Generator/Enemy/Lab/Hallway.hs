{-# LANGUAGE FlexibleContexts #-}

module Game.NetFlak.Generator.Enemy.Lab.Hallway
  ( generateLabHallwayEnemy
  ) where

import Control.Monad.Reader (MonadReader, ask)
import Control.Monad.Random (MonadRandom, getRandomR, getRandomRs)

import Data.Set (Set, notMember)

import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.Potion

import Lens.Simple

generateLabHallwayEnemy ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => Integer -> Set Integer -> m Enemy
generateLabHallwayEnemy bound occupiedHealthes = do
  difficulty <- view difficulty <$> ask
  enemyName <- generateName
  enemyDamage <- getRandomR (0, 1)
  let
    iconChar = case enemyName of
      (x : _) -> x
      _       -> '?'
    upperBound = 2 ^ (div difficulty 5) * (div bound 2)
    lowerBound = if difficulty < 15
      then 0
      else -(div upperBound 2)
  enemyHealth <- getUntil (flip notMember occupiedHealthes) $
    getRandomR (lowerBound, upperBound)
  effect <- choose
    [ Mirror
    , Noitop
    , GrowthPot
    , WitherPot
    , CollatzPot
    , WeakPot
    ]
  return Enemy
    { _iconChar      = iconChar
    , _iconPColor    = RedPID
    , _name          = enemyName
    , _enemyHealth   = enemyHealth
    , _enemyDamage   = enemyDamage
    , _enemyStack    = []
    , _enemyGrowth   = NoChange
    , _memories      = NoMovement
    , _ai            = Simple
    , _attackEffects = [effect]
    , _statueHealth  = Nothing
    }

generateName ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m String
generateName = choose
  [ "Body Without Organs"
  , "Lab Rat"
  , "Blob"
  , "Jelly"
  , "Automaton"
  ]

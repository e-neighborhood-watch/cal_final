{-# Language FlexibleContexts #-}

module Game.NetFlak.Generator.Level.StatueGarden.TrophyRoom (generateTrophyRoom) where

import Control.Monad 
import Control.Monad.Random.Class
import Control.Monad.Reader.Class

import Game.NetFlak.Generator.Enemy
import Game.NetFlak.Generator.Enemy.StatueGarden.Keeper
import Game.NetFlak.Generator.Util

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnvironmentVariables
import Game.NetFlak.Types.Level

import Lens.Simple

collatz :: (Num a, Integral a) => a -> a
collatz n
  | even n = div n 2
  | odd  n = 3 * n + 1

generateStatue ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => Integer -> m Enemy
generateStatue health = chooseM
    -- Generate an enemy at local difficulty and make it a statue
    [ set name "Guardian" <$>
      set statueHealth (Just health) <$>
        generateEnemy
    -- Generate another statue and modify it's statue health
    -- This makes it so using a potion doesn't make you safe
    , flip (liftM2 (over statueHealth)) (generateStatue health) $ fmap fmap $ choose
       [ (+1)
       , subtract 1
       , collatz
       ]
    ]
  

generateTrophyRoom ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m EndlessLevel
generateTrophyRoom = do
  let
    boxSize = 6
  keeper   <- generateKeeper
  distance <- (*2) <$> getRandomR (2, 5)
  statues  <- sequence $ do
    x <- [1, 3 .. 2 * boxSize + distance ]
    y <- [1, 3 .. 2 * boxSize ]
    return $ (,) (x, y) <$> generateStatue (view enemyHealth keeper)
  return Level
    { _levelSize       = (1 + 2 * boxSize + distance, 1 + 2 * boxSize)
    , _playerLocation  = (boxSize, boxSize)
    , _floorComponents = []
    , _floorPotions   = []
    , _enemies         = ((boxSize + distance, boxSize), keeper) : statues
    , _exitDoors       = [ Door False (-1, boxSize) LevelClear Never Nothing ]
    , _startMessages   = []
    }

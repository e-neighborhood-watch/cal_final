{-# Language FlexibleContexts #-}

module Game.NetFlak.Generator.Util
  ( choose
  , chooseM
  , getUntil
  , enemyDistance
  ) where

import Control.Monad.Random 
  ( join
  , uniform
  , MonadRandom
  , getRandomR
  )
import Control.Monad.Reader
  ( MonadReader
  , ask
  )
import Control.Monad.State.Lazy
  ( StateT
  )
import Control.Monad.Trans.Class

import Game.NetFlak.Types.EnvironmentVariables

import Lens.Simple
  ( view
  )

choose :: (Foldable t, MonadRandom m) => t a -> m a
choose = uniform

chooseM :: (Foldable t, MonadRandom m) => t (m a) -> m a
chooseM = join . choose

enemyDistance ::
  ( MonadRandom m
  , MonadReader EnvironmentVariables m
  )
    => m Integer
enemyDistance = do
  difficulty <- view difficulty <$> ask
  getRandomR (2, 3 + div difficulty 5)

getUntil :: (MonadRandom m) => (a -> Bool) -> m a -> m a
getUntil pred action = do
  result <- action
  if pred result
    then return result
    else getUntil pred action

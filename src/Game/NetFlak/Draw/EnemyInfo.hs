module Game.NetFlak.Draw.EnemyInfo (drawEnemyInfo) where

import Data.List (intercalate)
import Data.Char (toUpper, toLower)

import Graphics.Vty

import Game.NetFlak.Types.Enemy

import Game.NetFlak.Items
import Game.NetFlak.Window
import Game.NetFlak.Util

import Lens.Simple

reformat :: String -> String
reformat [] = []
reformat (a:b) = toUpper a : b ++ "."

trimPeriod :: String -> String
trimPeriod ""  = ""
trimPeriod "." = ""
trimPeriod (a:b) = a : trimPeriod b

deformat :: String -> String
deformat [] = []
deformat (a:b) = toLower a : trimPeriod b

enemyDesc :: Enemy -> [String]
enemyDesc enemy = concat
  [ [ "Deals " ++ show (view enemyDamage enemy) ++ " damage." ]
  , [ "Is killed by " ++ show (view enemyHealth enemy) ++ "." ]
  , case view enemyStack enemy of
    [] -> []
    startStack ->
      [ "Spells against it start with " ++ intercalate ", " (map show startStack) ++ " on the stack." ]
  , case view attackEffects enemy of
    [] -> []
    effects ->
      [ reformat (intercalate ", then " $ (deformat . itemDesc) <$> effects) ]
  ]

drawEnemyInfo :: Maybe Enemy -> Window
drawEnemyInfo (Just enemy) = imagesWindow Min Center $
  case (view statueHealth enemy) of
    Just health ->
      [ vertCat $
        [ string (defAttr `withForeColor` brightBlack) $ "Statue of a " ++ sanitize (view name enemy)
        , string defAttr $ " Awoken by " ++ show health ++ "."
        , string defAttr $ " Once awoken it:"
        ] ++ 
          ((string defAttr . ("  " ++)) <$> enemyDesc enemy)
      ]
    Nothing ->
      [ vertCat $
        (string (defAttr `withForeColor` red) $ sanitize $ view name enemy) 
          : ((string defAttr . (' ' :)) <$> enemyDesc enemy)
      ]
drawEnemyInfo Nothing = imagesWindow Min Center [emptyImage]


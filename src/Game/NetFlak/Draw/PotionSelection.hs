module Game.NetFlak.Draw.PotionSelection (drawPotionSelection) where

import Game.NetFlak.Types.Potion

import Game.NetFlak.Items
import Game.NetFlak.Color

import Graphics.Vty

drawPotionSelection :: (Num n, Eq n, Enum n) => n -> Image
drawPotionSelection n = vertCat $ do
  (potion, potionIndex) <- zip orderedPotions $ [0..]
  let
    attr = if n == potionIndex
      then
        defAttr `withStyle` standout
      else
        defAttr
  return $ horizJoin
   (string (attr `withForeColor` (getColor $ itemColor potion)) $ itemName potion) 
     (string defAttr " ")

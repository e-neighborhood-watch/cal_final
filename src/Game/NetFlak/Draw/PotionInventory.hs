module Game.NetFlak.Draw.PotionInventory (useItemInventoryWindow) where

import Control.Monad.State.Lazy (get)

import Data.Map.Strict (toList)

import Graphics.Vty

import Game.NetFlak.Types.World
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Inventory

import Game.NetFlak.Util (maybeIndex)
import Game.NetFlak.GenericImage (genericImageWidth)
import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Game.NetFlak.Draw.Utils

import Lens.Simple

useItemInventoryWindow :: Bool -> Integer -> World a -> WindowMonad [Image]
useItemInventoryWindow selected cursorIndex world = do
  (_, (width, _)) <- get
  setPicture $ drawPotionInventory width selected cursorIndex world

drawPotionInventory :: Integer -> Bool -> Integer -> World a -> Window
drawPotionInventory width selected cursorIndex world = imagesWindow Min Min $
  pure $ vertCat $                                                           -- Combine vertically
    zipWith drawCursor [0..] $                                               -- handle the cursor
      map (extendHighlight (width - 2) . imgFromPotion (width - 2) world) $ -- Create images
        potionList
  where
    potionList = toList $ view (player . playerInv . heldPotions) world
    buffer ix
      | view (player . playerInv . quickPotion) world == maybeIndex (fst <$> potionList) ix = "U "
      | otherwise = "  "
    drawCursor ix
      | ix == cursorIndex
      , selected
        = (string (defAttr `withStyle` standout) (buffer ix) <|>)
      | otherwise
        = (string defAttr (buffer ix) <|>)

module Game.NetFlak.Draw.Level (
    drawLevel
  , simpleDrawLevel
  ) where

import Graphics.Vty

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.World

import Game.NetFlak.Draw.Utils

import Game.NetFlak.Color
import Game.NetFlak.Enemy
import Game.NetFlak.Items
import Game.NetFlak.Level
import Game.NetFlak.Util

import Lens.Simple

biliftA2 :: (a -> b -> c) -> (a, a) -> (b, b) -> (c, c)
biliftA2 f (a1, a2) (b1, b2) = (f a1 b1, f a2 b2)

addPlayer :: Level a -> [Image] -> [Image]
addPlayer lvl = addCharAt (defAttr `withForeColor` cyan) '@' $ (1,1) + view playerLocation lvl

addEnemies :: Level a -> [[Image] -> [Image]]
addEnemies lvl =
  [ addCharAt (defAttr `withForeColor` (getColor $ iconColorPID enemy)) (sanitizeChar $ view iconChar enemy) $ (1, 1) + loc
  | (loc, enemy) <- view enemies lvl
  , inBounds lvl loc
  ]

addCorpses :: World a -> [[Image] -> [Image]]
addCorpses world =
  [ addCharAt (defAttr `withForeColor` red `withStyle` standout) (view iconChar enemy) $ (1, 1) + loc
  | (loc, enemy) <- view deadEnemies world
  , inBounds (view currentLevel world) loc
  ]

addComps :: Level a -> [[Image] -> [Image]]
addComps lvl =
  [ case item of
    UnlimitedSC _ -> addCharAt (defAttr `withForeColor` green) (itemIcon item) $ (1, 1) + loc
    LimitedSC _ _ -> addCharAt (defAttr `withForeColor` blue)  (itemIcon item) $ (1, 1) + loc
  | (loc, item) <- view floorComponents lvl
  , inBounds lvl loc
  ]

addPotions :: Level a -> [[Image] -> [Image]]
addPotions lvl =
  [ addCharAt (defAttr `withForeColor` (getColor $ itemColor item)) (itemIcon item) $ (1, 1) + loc
  | (loc, item) <- view floorPotions lvl
  , inBounds lvl loc
  ]

addDoor :: Level a -> Door a -> [Image] -> [Image]
addDoor lvl door
  | not $ reachable lvl doorLoc = id
  | view isOpen door            = addCharAt defAttr '┼' $ doorLoc - biliftA2 min (-1, -1) (view playerLocation lvl)
  | otherwise                   = id
  where
    doorLoc = view doorPosition door

addDoors :: Level a -> [Image] -> [Image]
addDoors lvl = foldr (.) id $ addDoor lvl <$> view exitDoors lvl

addThings :: Level a -> [[Image] -> [Image]]
addThings lvl = addEnemies lvl ++ addComps lvl ++ addPotions lvl

emptyRoom :: Level a -> Image
emptyRoom lvl = vertJoin horizWall $ vertJoin (horizJoin vertWall $ horizJoin floor vertWall) horizWall
  where
    (lvlSizeA, lvlSizeB) = view levelSize lvl
    horizWall = charFill defAttr ' ' (lvlSizeB + 2) 1
    vertWall  = charFill defAttr ' ' 1 lvlSizeA
    floor     = charFill defAttr '.' lvlSizeB lvlSizeA

drawLevel :: World a -> [Image]
drawLevel world = addPlayer lvl $ addDoors lvl filledFloor
  where
    lvl = view currentLevel world
    filledFloor          = foldr ($) [ emptyRoom lvl ] $ addThings lvl ++ addCorpses world

-- A version of drawLevel that does not require a world type
-- Does not draw corpses since corpses are stored in the world
simpleDrawLevel :: Level a -> [Image]
simpleDrawLevel lvl = addPlayer lvl $ addDoors lvl filledFloor
  where
    filledFloor          = foldr ($) [ emptyRoom lvl ] $ addThings lvl

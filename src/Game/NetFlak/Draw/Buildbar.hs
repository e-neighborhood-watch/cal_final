module Game.NetFlak.Draw.Buildbar (drawBuildbar) where

import Game.NetFlak.Types.BuildState
import Game.NetFlak.Types.Vector

import Graphics.Vty

modes :: [String]
modes = ["Player", "Size", "Door", "Add", "Edit", "Remove"]

indexFromUI :: BuildUI -> Maybe Int
indexFromUI MovingPlayer         = Just 0
indexFromUI ResizeLevel          = Just 1
indexFromUI MovingDoor           = Just 2
indexFromUI (Adding _)           = Just 3
indexFromUI Editing              = Just 4
indexFromUI Removing             = Just 5
indexFromUI _ = Nothing

drawBuildbar :: BuildUI -> Image
drawBuildbar mode = foldl horizJoin emptyImage $ do
  (modeName, modeIndex) <- zip modes $ Just <$> [0..]
  let
    attr = if indexFromUI mode == modeIndex
      then
        defAttr `withStyle` standout
      else
        defAttr
  return $ horizJoin (string attr modeName) (string defAttr " ")
  

module Game.NetFlak.Enemy
  ( separateStatues
  , enemyPriority
  , iconColorPID
  , totalDamage
  , getMemory
  , enemyAct
  ) where

import Prelude hiding (lookup)

import Data.List (sortBy, partition)
import Data.Map (Map(..), insertWith, lookup)
import Data.Maybe (isJust)

import Lens.Simple

import Game.NetFlak.Color
import Game.NetFlak.RichString
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.EnemyMemory
import Game.NetFlak.Types.PseudoColor
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Vector
import Game.NetFlak.Util

flipOrder :: Ordering -> Ordering
flipOrder LT = GT
flipOrder EQ = EQ
flipOrder GT = LT

separateStatues :: [(a, Enemy)] -> ([(a, Enemy)], [(a, Enemy)])
separateStatues = partition (isJust . view (_2 . statueHealth))

-- Used for setting priority at the beginning of the level
-- Enemies with higher priority get to choose their moves first
enemyPriority :: (a, Enemy) -> (a, Enemy) -> Ordering
enemyPriority (_, e1) (_, e2) = flipOrder $ compare (view enemyDamage e1) (view enemyDamage e2) 

iconColorPID :: Enemy -> PseudoColorID
iconColorPID enemy = case view statueHealth enemy of
  Just _  -> GreyPID
  Nothing -> view iconPColor enemy

totalDamage :: [EnemyAction] -> Integer
totalDamage = sum . fmap toDmg
  where toDmg (AttackAction dmg _) = dmg
        toDmg _ = 0

getMemory :: Vector -> Vector -> EnemyMemory
getMemory (lastA, lastB) (nextA, nextB)
  | lastA /= nextA = Vertical
  | lastB /= nextB = Horizontal
  | otherwise      = NoMovement

enemyAct :: EnemyAction -> (Vector, Enemy) -> (Vector, Enemy)
enemyAct (MoveAction dest)        (curr, enemy) = (dest, set memories (getMemory curr dest) enemy)
enemyAct (GrowthAction newHealth) (curr, enemy) = (curr, set enemyHealth newHealth enemy)
enemyAct _ enemy = enemy


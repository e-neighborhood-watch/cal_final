module Game.NetFlak.TakeStep
  ( movePlayer
  , takeStep
  , useHotbarComponent
  , executeSpell
  ) where

import Prelude hiding (lookup)

import Lens.Simple

import Control.Monad (guard)

import Data.List (partition, genericLength, genericIndex)

import Game.NetFlak.Enemy
import Game.NetFlak.Enemy.AI
import Game.NetFlak.Items
import Game.NetFlak.Level
import Game.NetFlak.Logs
import Game.NetFlak.RichString
import Game.NetFlak.Spell
import Game.NetFlak.World

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.LogItem
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.RichString
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Data.Map (lookup)

movePlayer :: Vector -> World a -> World a
movePlayer shift world
 -- Cannot move into an enemy
 | elem newLocation $ fst <$> view enemies lvl = world
 -- Can move out of bounds if into an open door
 | elem newLocation
   [ view doorPosition door
   | door <- view exitDoors lvl
   , view isOpen door
   ]                                           = takeStep newWorld
 -- Cannot move out of bounds otherwise
 | not $ inBounds lvl newLocation              = world
 | otherwise                                   = takeStep newWorld
 where
    lvl         = view currentLevel world
    newWorld    = over (currentLevel . playerLocation) (shift +) world
    newLocation = view (currentLevel . playerLocation) newWorld

performAttack :: EnemyAction -> World a -> World a
performAttack (AttackAction damage potions) world =
  over (player . damageTaken) (+ damage) $
    foldr applyPotion world potions
performAttack _ world = world

openDoors :: Level a -> Level a
openDoors lvl
  | isWon lvl = over (exitDoors . mapped) (open LevelClear . open Always) lvl
  | otherwise = over (exitDoors . mapped) (open Always) lvl
  where
    open :: DoorCondition -> Door a -> Door a
    open cond door
      | cond == view openCondition door = set isOpen True door
      | otherwise                         = door

takeStep :: World a -> World a
takeStep world =
  -- Open any doors that need to be opened
  over currentLevel openDoors $
    -- Pick up items at the players feet
    -- Should this be done in movePlayer instead?
    performPickUp $
      case view (player . effectStack) world of
        -- If the player is out of turns let the enemies move
        0 ->
          -- Perform attacks
          flip (foldr performAttack) enemyActions $
            -- Replace old enemies with new enemies
            -- New enemies have updated locations, some may have died etc.
            set (currentLevel . enemies) newEnemies $
              over rawLog (actionLogs ++) $
                -- Kill the trampled enemies
                over deadEnemies (++ trampledEnemies) $
                  world
        -- If the player has more than one extra turn don't do stuff with enemies
        _ ->
          over (player . effectStack) (subtract 1) $
            world
  where
    enemyList = view (currentLevel . enemies) world
    enemyActions = getEnemyActions $ view currentLevel world

    died :: EnemyAction -> Bool
    died (Death _) = True
    died _ = False

    -- Trample enemies
    newEnemies = uncurry enemyAct <$> filter (not . died . fst) (zip enemyActions enemyList)
    trampledEnemies = snd <$> filter (died . fst) (zip enemyActions enemyList)

    -- Get logs from enemy action
    actionLogs =
      getDoorMessages (set enemies newEnemies $ view currentLevel world) ++
        [ PlayerDies
        | view (player . maxHealth) world <= view (player . damageTaken) world + totalDamage enemyActions
        ] ++
          concat ( zipWith logAction enemyActions $ fmap snd enemyList )

useHotbarComponent :: Char -> World a -> World a
useHotbarComponent key world
  | Just index <- lookup key $ view (player . playerInv . hotbarMap) world  -- hotkey has an index
  , index >= 0                                                              -- index is non-negative (should always pass)
  , index < genericLength comps                                             -- index corresponds to a component
    = let comp = genericIndex comps index in
      case canUse comp world of
        Nothing -> takeStep $ over currentSpell (addComponent comp) world
        Just log -> over rawLog (log :) world
  | otherwise = world
  where
    comps = view (player . playerInv . heldComponents) world

executeSpell :: World a -> World a
executeSpell world =
  case partitionCasualties (view currentSpell world) (view (userSettings . maxSpellCycles) world) (view (currentLevel . enemies) world) of
    Left error -> over rawLog (error :) world
    Right ((casualties, survivors), logs) ->
      takeStep $ 
        set (currentLevel . enemies) survivors $
          over deadEnemies (casualties ++) $
            over rawLog (logs ++) $
              world

{-# Language NoMonomorphismRestriction#-}
module Game.NetFlak.Render.Play (renderPlay) where

import Control.Monad (guard)
import Control.Monad.State.Lazy (get)

import Data.Foldable (foldMap)
import Data.List (genericLength, genericIndex, intersperse)
import Data.Map.Strict (toList)
import Data.Maybe (fromMaybe)

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Inventory
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.PlayState
import Game.NetFlak.Types.Settings
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Game.NetFlak.Draw.Box
import Game.NetFlak.Draw.CompInventory
import Game.NetFlak.Draw.Controls
import Game.NetFlak.Draw.Enemybar
import Game.NetFlak.Draw.EnemyInfo
import Game.NetFlak.Draw.Hotbar
import Game.NetFlak.Draw.HP
import Game.NetFlak.Draw.ItemInfo
import Game.NetFlak.Draw.Level
import Game.NetFlak.Draw.Lookable
import Game.NetFlak.Draw.LookingAt
import Game.NetFlak.Draw.Messages
import Game.NetFlak.Draw.Settings
import Game.NetFlak.Draw.Spellbar
import Game.NetFlak.Draw.PotionInventory
import Game.NetFlak.Draw.USlot
import Game.NetFlak.Draw.Utils

import Game.NetFlak.Color
import Game.NetFlak.Enemy
import Game.NetFlak.GenericImage
import Game.NetFlak.Items
import Game.NetFlak.Level (enemiesInBounds, inBounds)
import Game.NetFlak.RichString
import Game.NetFlak.Util (unbalanced, maybeIndex)
import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Graphics.Vty

import Lens.Simple

-- Takes a vector and halves it
centerOf :: Vector -> Vector
centerOf = ((,) . (flip div 2) . fst) <*> ((flip div 2) . snd) 

centerImage :: Vector -> Vector -> Image -> Image
centerImage targetSize relCenter = positionImage targetSize $ (centerOf targetSize) - relCenter

centerPicture :: Vector -> Vector -> [Image] -> [Image]
centerPicture targetSize center = fmap $ centerImage targetSize center

renderPlay ::
  Lookable link 
    => Vector -> (UIState gen link, World link) -> Picture
renderPlay screenSize (WalkMode, world) =
  picForLayers $ extractImages screenSize $ do
    -- Message bar
    consumeBottom (view (userSettings . messageWidth) world) $
      setPicture $ drawMessages 0 world

    -- Spellbar
    consumeBottom (view (userSettings . spellbarWidth) world) $ do
      setPicture $ drawSpellbar world
  
    -- Hotbar
    consumeLeft (view (userSettings . hotbarWidth) world) $ do
      consumeBottom 3 $ do
        uSlotWindow world
      hotbarWindow world
  
    -- Enemybar
    consumeRight (view (userSettings . enemybarWidth) world) $ do
      consumeTop 3 $ setPicture $ drawHP world
      setPicture $ imagesWindow Min Min $ [ drawEnemybar Nothing world ]
  
    -- Map
    let (centerA, centerB) = (1, 1) + transpose (view (currentLevel . playerLocation) world)
    setPicture $ imagesWindow (RelativeCenter (max 0 centerA)) (RelativeCenter (max 0 centerB)) $
        drawLevel world

renderPlay screenSize (EnemyMode index, world) =
  picForLayers $ extractImages screenSize $ do
    let
      maybeEnemy = maybeIndex (enemiesInBounds $ view currentLevel world) index

    -- Enemy info box
    consumeBottom (view (userSettings . infobarWidth) world) $ do
      setPicture $ drawEnemyInfo $ snd <$> maybeEnemy

    -- Enemybar
    consumeRight (view (userSettings . enemybarWidth) world) $ do
      consumeTop 3 $ setPicture $ drawHP world
      setPicture $ imagesWindow Min Min $ [ drawEnemybar (Just index) world ]

    let
      (centerA, centerB) = (1, 1) + transpose (
        fromMaybe (view (currentLevel . playerLocation) world) $ fmap fst maybeEnemy)

      highlightAttr :: Enemy -> Attr
      highlightAttr enemy = defAttr `withForeColor` (getColor $ iconColorPID enemy) `withStyle` standout

      highlightEnemy :: (Vector, Enemy) -> [Image] -> [Image]
      highlightEnemy (loc, enemy) = if inBounds (view currentLevel world) loc
        then addCharAt (highlightAttr enemy) (view iconChar enemy) $ (1, 1) + loc
        else id

    -- Map
    setPicture $ imagesWindow (RelativeCenter (max 0 centerA)) (RelativeCenter (max 0 centerB)) $
      fromMaybe id (fmap highlightEnemy maybeEnemy) (drawLevel world)

renderPlay screenSize (LookMode center, world) =
  picForLayers $ extractImages screenSize $ do
    -- Look info box
    consumeBottom (view (userSettings . infobarWidth) world) $ lookingAtWindow center world

    let
      newCenter@(newCenterA, newCenterB) = (1, 1) + transpose center

    -- Map
    setPicture $ imagesWindow (RelativeCenter (max 0 newCenterA)) (RelativeCenter (max 0 newCenterB)) $
      addCharAt (defAttr `withForeColor` green) '+' ((1, 1) + center) $
        drawLevel world

renderPlay screenSize (InventoryMode cursorLeft ix, world) =
  picForLayers $ extractImages screenSize $ do
    -- Message bar
    consumeBottom (view (userSettings . messageWidth) world) $
      setPicture $ drawMessages 0 world

    -- Infobar
    consumeBottom (view (userSettings . infobarWidth) world) $
      setPicture $
        case cursorLeft of
          True  -> drawItemInfo $ maybeIndex (view (player . playerInv . heldComponents) world) ix
          False -> drawItemInfo $ fst <$> maybeIndex (toList $ view (player . playerInv . heldPotions) world) ix

    -- Spellbar
    consumeBottom (view (userSettings . spellbarWidth) world) $
      setPicture $ drawSpellbar world

    -- Enemybar
    consumeRight (view (userSettings . enemybarWidth) world) $ do
      consumeTop 3 $ setPicture $ drawHP world
      setPicture $ imagesWindow Min Min $ [ drawEnemybar Nothing world ]

    -- Inventory banner
    consumeTop 2 $ setPicture $ imagesWindow Min Min $ [string defAttr "Inventory"]

    (_, (width, _)) <- get

    -- Inventory menu
    consumeLeft (div width 2) $
      compInventoryWindow cursorLeft ix world

    useItemInventoryWindow (not cursorLeft) ix world

renderPlay screenSize (SettingsMode index, world) = picForLayers $ constructWindow screenSize $ drawSettings index world

renderPlay screenSize (MessagesMode n, world) = picForLayers $ constructWindow screenSize $ drawMessages n world

renderPlay screenSize (ControlsMode, world) = picForLayers $ constructWindow screenSize $ drawControls world

renderPlay screenSize (Prompt message bkgdUI outgoing,world) = bkgdImages { picLayers = messageBoxImgs ++ picLayers bkgdImages }
  where
    messageBoxImgs = constructWindow screenSize $ imagesWindow Center Center
      [genericTranslate 2 1 textImg, drawBox $ genericImageSize textImg + (4,2)]
    textImg =
      vertCat (fmap richString message)
      <->
      genericTranslateY 1 (string defAttr $ intersperse '/' $ fmap fst outgoing)
    bkgdImages = renderPlay screenSize (bkgdUI,world)

renderPlay _ (Quit,_) = emptyPicture


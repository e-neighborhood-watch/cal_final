module Game.NetFlak.Render.Build (renderBuilder) where

import Data.List (genericIndex)

import Game.NetFlak.Types.BuildState
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Potion
import Game.NetFlak.Types.Vector

import Game.NetFlak.Draw.AddingBuildbar
import Game.NetFlak.Draw.Buildbar
import Game.NetFlak.Draw.Level
import Game.NetFlak.Draw.PotionSelection

import Game.NetFlak.Window
import Game.NetFlak.WindowMonad

import Graphics.Vty

import Lens.Simple

centerAtPlayer :: Level a -> WindowMonad [Image]
centerAtPlayer lvl = do
    let (centerA, centerB) = (1, 1) + transpose (view playerLocation lvl)
    setPicture $
      imagesWindow (RelativeCenter (max 0 centerA)) (RelativeCenter (max 0 centerB)) $
        simpleDrawLevel lvl 

renderBuilder :: Vector -> (BuildUI, Level a) -> Picture
renderBuilder screenSize (ui@MovingPlayer, lvl) =
  picForLayers $ extractImages screenSize $ do
    consumeTop 1 $ 
      setPicture $ imagesWindow Min Min [ drawBuildbar ui ]

    centerAtPlayer lvl

renderBuilder screenSize (ui@MovingDoor, lvl) =
  picForLayers $ extractImages screenSize $ do
    consumeTop 1 $ 
      setPicture $ imagesWindow Min Min [ drawBuildbar ui ]

    -- Center at door
    -- TODO no longer works due to multiple doors
    let (centerA, centerB) = (1, 1) + transpose (0, 0)
    setPicture $
      imagesWindow (RelativeCenter centerA) (RelativeCenter centerB) $
        simpleDrawLevel lvl 

renderBuilder screenSize (ui@ResizeLevel, lvl) =
  picForLayers $ extractImages screenSize $ do
    consumeTop 1 $ 
      setPicture $ imagesWindow Min Min [ drawBuildbar ui ]

    -- Center at bottom right corner of the level
    let (centerA, centerB) = (1, 1) + transpose (view levelSize lvl)
    setPicture $
      imagesWindow (RelativeCenter (max 0 centerA)) (RelativeCenter (max 0 centerB)) $
        simpleDrawLevel lvl 

renderBuilder screenSize (ui@(Adding a), lvl) = picForLayers $ extractImages screenSize $ do
  consumeTop 1 $ 
    setPicture $ imagesWindow Min Min [ drawBuildbar ui ]

  consumeTop 1 $
    setPicture $ imagesWindow Min Min [ drawAddingBuildbar $ a ]

  case a of
    Just (Potion (Just potUI)) -> case potUI of
      Select index ->
        setPicture $
          imagesWindow Min Min [ drawPotionSelection index ]
      Place item location -> do
        -- Center at new potion
        let (centerA, centerB) = (1, 1) + transpose location
        -- TODO draw this on top instead of adding it to the inventory
        setPicture $
          imagesWindow (RelativeCenter (max 0 centerA)) (RelativeCenter (max 0 centerB)) $
            simpleDrawLevel $ over floorPotions ((location, item) :) lvl 
    _ -> do
      centerAtPlayer lvl

-- Should never actually render
-- Just render as moving player for now
renderBuilder screenSize (QuitBuild, lvl) = renderBuilder screenSize (MovingPlayer, lvl)
-- Place holder for any UI states that are not defined
-- TODO make this render a warning
renderBuilder screenSize (ui, lvl) =
  picForLayers $ extractImages screenSize $ do
    consumeTop 1 $ 
      setPicture $ imagesWindow Min Min [ drawBuildbar ui ]

    centerAtPlayer lvl

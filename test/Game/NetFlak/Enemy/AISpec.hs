{-# Language ScopedTypeVariables #-}
module Game.NetFlak.Enemy.AISpec (spec) where

import Control.Monad.Random.Class

import Game.NetFlak.ArbitraryInstances.Level
import Game.NetFlak.ArbitraryInstances.Enemy

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.EnemyAction
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Vector

import Game.NetFlak.Enemy.AI

import Game.NetFlak.Gen

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "getEnemyActions" $ do
    it "chooses attacks that match an enemy's stats" $ property $
      \ (lvl :: PuzzleLevel) ->
        sequence_
          [ do
            dmg     `shouldBe` view enemyDamage   enemy
            effects `shouldBe` view attackEffects enemy
          | ((loc, enemy), AttackAction dmg effects) <- zip (view enemies lvl) $ getEnemyActions lvl
          ]
    it "only attacks if adjacent" $ property $
      \ (lvl :: PuzzleLevel) ->
        sequence_
          [ (loc, view playerLocation lvl) `shouldSatisfy` ((< 2) . uncurry manhattan)
          | ((loc, _), AttackAction _ _) <- zip (view enemies lvl) $ getEnemyActions lvl
          ]
    it "never moves an enemy more than one space" $ property $
      \ (lvl :: PuzzleLevel) ->
        sequence_
          [ (loc, newLoc) `shouldSatisfy` ((< 2) . uncurry manhattan)
          | ((loc, enemy), MoveAction newLoc) <- zip (view enemies lvl) $ getEnemyActions lvl
          ]
  describe "bestAStarMove" $ do
    it "returns Nothing if start and dest are the same" $ property $
      \ graph ->
        \ loc -> bestAStarMove (applyFun graph) loc loc `shouldBe` Nothing

module Game.NetFlak.Draw.LookableSpec (spec) where

import Game.NetFlak.Draw.Lookable

import Game.NetFlak.Types.Door
import Game.NetFlak.Types.SpecialArea

import Game.NetFlak.ArbitraryInstances.Door
import Game.NetFlak.ArbitraryInstances.SpecialArea

import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "doorDescription" $ do
    context "when it receives a ()" $
      it "ends with a period" $
         forAll (arbitrary :: Gen (Door ())) $
           \ link ->
             last (doorDescription link) `shouldBe` '.'
    context "when it receives a Maybe SpecialArea" $
      it "ends with a period" $
         forAll (arbitrary :: Gen (Door (Maybe SpecialArea))) $
           \ link ->
             last (doorDescription link) `shouldBe` '.'

{-# Language InstanceSigs #-}

module Game.NetFlak.Gen where

import Control.Monad.Random.Class

import System.Random

import Test.QuickCheck

instance MonadRandom Gen where
  getRandomR  :: Random a => (a, a) -> Gen a
  getRandomR  = choose
  getRandom   :: Random a => Gen a
  getRandom   = fst . random . mkStdGen <$> arbitrary
  getRandomRs :: Random a => (a, a) -> Gen [a]
  getRandomRs = listOf . getRandomR
  getRandoms  :: Random a => Gen [a]
  getRandoms  = listOf getRandom

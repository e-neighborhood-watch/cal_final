module Game.NetFlak.SpellSpec (spec) where

import System.IO.Unsafe

import Lens.Simple (view)

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.ArbitraryInstances.Spell
import Game.NetFlak.Spell
import Game.NetFlak.Items
import Game.NetFlak.Types.Spell
import Game.NetFlak.Types.SpellComponent
import Game.NetFlak.TestHelpers

spec :: Spec
spec = do
  describe "doDeletes" $ do
    it "never leaves an X" $
      forAll spellStrings $
        \string ->
           notElem 'X' $ doDeletes string
    it "deletes a character with X unless it is empty" $
      forAll spellStrings $
        \string ->
           null (doDeletes string) ||
             init (doDeletes string) == doDeletes (string ++ "X")
    it "changes nothing to add an X to the front" $
      forAll spellStrings $
        \string ->
          doDeletes string == doDeletes ('X' : string)
    it "changes nothing to add Xs to the front" $
      forAll spellStrings $
        \string ->
          forAll (listOf (elements "X") `suchThat` ((1 >) . length)) $
            \xs ->
              doDeletes string == doDeletes (xs ++ string)
    it "a Y on the end remains" $
      forAll spellStrings $
        \string ->
          doDeletes string ++ "Y" == doDeletes (string ++ "Y")
  describe "addComponent" $ do
    it "does the same change to the spell contents regardless of whether the component is limited or unlimited" $
      forAll (arbitrary `suchThat` (>0)) $
        \n -> \structure -> \spell ->
          view spellContents (addComponent (UnlimitedSC structure) spell) ==
            view spellContents (addComponent (LimitedSC n structure) spell)
    context "when using an unlimited component" $
      it "doesn't change the used components" $ property $
        \structure -> \spell ->
          view usedComps (addComponent (UnlimitedSC structure) spell) ==
            view usedComps spell
    context "when using an limited component" $
      it "does change the used components when using a limited component" $
        forAll (arbitrary `suchThat` (>0)) $
          \n -> \structure -> \spell ->
            view usedComps (addComponent (LimitedSC n structure) spell) /=
              view usedComps spell

module Game.NetFlak.ArbitraryInstances.Potion where

import Game.NetFlak.Types.Potion

import Test.QuickCheck

instance Arbitrary Potion where
   arbitrary = elements orderedPotions


module Game.NetFlak.ArbitraryInstances.Level where

import Control.Monad (liftM2)

import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.ArbitraryInstances.Door
import Game.NetFlak.ArbitraryInstances.Enemy
import Game.NetFlak.ArbitraryInstances.SpellComponent
import Game.NetFlak.ArbitraryInstances.Potion
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Vector

import Test.QuickCheck

instance Arbitrary a => Arbitrary (Level a) where
  arbitrary = do
    n <- genericGetSize `suchThat` (>0)
    -- Get a size for the level
    aSize   <- choose (1, n)
    bSize   <- choose (1, n)
   
    verExit <- choose (0, aSize - 1)
    horExit <- choose (0, bSize - 1)

    -- Should this inforce that the door is reachable?
    exits <- arbitrary

    let
      arbCoord :: Gen Vector
      arbCoord = do
        a <- choose (0, aSize - 1)
        b <- choose (0, bSize - 1)
        return (a,b)

    player <- arbCoord

    enemies <- listOf $ liftM2 (,) arbCoord arbitrary

    floorPotions <- listOf $ liftM2 (,) arbCoord arbitrary

    floorComps <- listOf $ liftM2 (,) arbCoord arbitrary

    return $ Level {
       _levelSize = (aSize, bSize)
     , _playerLocation = player
     , _enemies = enemies
     , _exitDoors = exits
     , _floorComponents = floorComps
     , _floorPotions = floorPotions
     -- TODO random start messages
     , _startMessages = []
     }


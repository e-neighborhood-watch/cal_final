module Game.NetFlak.ArbitraryInstances.Settings where

import Game.NetFlak.Types.Settings

import Test.QuickCheck

-- TODO make this actually arbitrary
instance Arbitrary Settings where
  arbitrary = do
    permadeath <- arbitrary
    return $ Settings {
        _maxSpellCycles = 10000
      , _userControls = []
      , _hotbarWidth = 15
      , _messageWidth = 15
      , _enemybarWidth = 15
      , _spellbarWidth = 15
      , _infobarWidth = 15
      , _hotkeys = "1234567890"
      , _permadeath = permadeath
      }
      

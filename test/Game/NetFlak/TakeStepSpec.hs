{-# Language ScopedTypeVariables #-}
module Game.NetFlak.TakeStepSpec (spec) where

import Control.Monad (liftM2, guard)

import Data.Maybe (isNothing)

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Game.NetFlak.Level (inBounds)
import Game.NetFlak.TakeStep
import Game.NetFlak.TestHelpers

import Game.NetFlak.Types.Enemy
import Game.NetFlak.Types.Level
import Game.NetFlak.Types.Player
import Game.NetFlak.Types.Vector
import Game.NetFlak.Types.World

import Game.NetFlak.ArbitraryInstances.Enemy
import Game.NetFlak.ArbitraryInstances.Tools
import Game.NetFlak.ArbitraryInstances.World

-- TODO use a writer Monad
-- O(n^2) could be O(n log n)
containsNoDuplicates :: (Eq a, Show a) => [a] -> IO ()
containsNoDuplicates [] = return ()
containsNoDuplicates (x : xs)
  | elem x xs = expectationFailure ("Contains " ++ show x  ++ " more than once") >> containsNoDuplicates xs
  | otherwise = containsNoDuplicates xs

enemiesOutOfBoundsOf :: World a -> Gen [(Vector, Enemy)]
enemiesOutOfBoundsOf world = listOf $ do
  n <- genericGetSize `suchThat` (>0)
  
  coord <- (liftM2 (,) <*> id) (choose (-n, n)) `suchThat`
    (not . inBounds (view currentLevel world))

  enemy <- arbitrary :: Gen Enemy
 
  return $ (coord, enemy)

spec :: Spec
spec = do
  describe "takeStep" $ do
    context "when the player is out of actions" $ do
      -- TODO make this work for statues
      it "never leaves two enemies in the same space" $
        forAll (arbitrary `suchThat` ((== 0) . view (player . effectStack)) :: Gen (World ())) $
          containsNoDuplicates . map fst . filter (isNothing . view (_2 . statueHealth)) . view (currentLevel . enemies) . takeStep
      it "never leaves an enemy where the player is" $
        forAll (arbitrary `suchThat` ((== 0) . view (player . effectStack)) :: Gen (World ())) $
          \ world -> 
            let newWorld = takeStep world in
              notElem (view (currentLevel . playerLocation) newWorld) $ map fst $
                view (currentLevel . enemies) newWorld
      it "kills all enemies out of bounds" $
        forAll (arbitrary `suchThat` ((== 0) . view (player . effectStack)) :: Gen (World ())) $
          \ world ->
            forAll (enemiesOutOfBoundsOf world) $
              \ newEnemies ->
                 view (currentLevel . enemies) (takeStep world) `shouldBe` view (currentLevel . enemies) (takeStep $ over (currentLevel . enemies) (++ newEnemies) world)
    context "when the player is not out of actions" $ do
      it "decreases the number of actions left by 1" $ property $
        forAll (arbitrary `suchThat` ((> 0) . view (player . effectStack)) :: Gen (World ())) $
          \ world ->
            view (player . effectStack) (takeStep world) `shouldBe` view (player . effectStack) world - 1
      it "does not move enemies" $ property $
        forAll (arbitrary `suchThat` ((> 0) . view (player . effectStack)) :: Gen (World ())) $
          \ world ->
            (fst <$> view (currentLevel . enemies) world) `shouldBe` (fst <$> view (currentLevel . enemies) (takeStep world))
      it "does not damage the player" $ property $
        forAll (arbitrary `suchThat` ((> 0) . view (player . effectStack)) :: Gen (World ())) $
          \ world ->
            view (player . damageTaken) world `shouldBe` view (player . damageTaken) (takeStep world)
    it "never allows the number of remaining player moves to be negative" $ property $
      \ (world :: World ()) ->
        view (player . effectStack) (takeStep world) `shouldSatisfy` (>= 0)
    it "never moves an enemy out of bounds" $ property $
      \ (world :: World ()) ->
        let newWorld = takeStep world in
          view (currentLevel . enemies) (takeStep world) `shouldAllSatisfy` (inBounds (view currentLevel newWorld) . fst)
    it "doesn't move the player" $ property $
      \ (world :: World ()) ->
        view (currentLevel . playerLocation) world `shouldBe` view (currentLevel . playerLocation) (takeStep world)
    it "picks up all components the player is standing on" $ property $
      \ (world :: World ()) ->
         notElem (view (currentLevel . playerLocation) world) $ map fst (view (currentLevel . floorComponents) (takeStep world))

    it "picks up all use items the player is standing on" $ property $
      \ (world :: World ()) ->
        notElem (view (currentLevel . playerLocation) world) $ map fst (view (currentLevel . floorPotions) (takeStep world))
    it "doesn't destroy enemies" $ property $
      \ (world :: World ()) ->
        let nextWorld = takeStep world in
          length (view deadEnemies world ++ view (currentLevel . enemies) world)
            `shouldBe`
              length (view deadEnemies nextWorld ++ view (currentLevel . enemies) nextWorld)

module Game.NetFlak.Generator.Enemy.Lab.HallwaySpec (spec) where

import Game.NetFlak.ArbitraryInstances.EnvironmentVariables

import Game.NetFlak.Generator.Enemy.Lab.Hallway

import Game.NetFlak.Gen

import Game.NetFlak.Types.Enemy

import Lens.Simple

import Test.Hspec
import Test.QuickCheck

import Control.Monad.Trans.Reader (runReaderT)

spec :: Spec
spec = do
  describe "generateHallwayEnemy" $
    it "never names an enemy the empty string" $
      forAll (do
        bound <- arbitrary
        occupiedHealthes <- arbitrary
        arbitrary >>= runReaderT (generateLabHallwayEnemy bound occupiedHealthes)) $
        \ enemy ->
          view name enemy `shouldNotBe` ""
